import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MaterialApp',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.purple[800],
        accentColor: Colors.purple[600],
        // Define the default font family.
        fontFamily: 'Georgia',
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Vista Avatar', style: TextStyle(fontSize: 25)),
              Icon(
                Icons.directions_bike,
                color: Colors.limeAccent[50],
                size: 75.0
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.backspace),
          onPressed: ()=>{
            Navigator.pop(context)
        }),
      ),
    );
  }
}