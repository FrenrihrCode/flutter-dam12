import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';

class HomePage extends StatelessWidget {
  /*
  final TextStyle styleText = TextStyle(color: Colors.black, fontWeight: FontWeight.bold);
  final data = [ {'titulo': 'Alertas', 'icono': Icons.add_alert, 'color': Colors.blue[200]}, {'titulo': 'Avatares', 'icono': Icons.person_pin, 'color': Colors.blue[400]}, 
    {'titulo': 'Contenedor', 'icono': Icons.table_chart, 'color': Colors.green[200]}, {'titulo': 'Contenedor', 'icono': Icons.content_paste, 'color': Colors.green[400]}, 
    {'titulo': 'Entradas', 'icono': Icons.input, 'color': Colors.red[200]}, {'titulo': 'Sliders', 'icono': Icons.grid_on, 'color': Colors.purple[200]}, {'titulo': 'Opciones varias', 'icono': Icons.list, 'color': Colors.red[400]},
  ];
  
  List<Widget> _crearItemsCorta1(context) {
    return data.map((item) {
      return Column(children: <Widget>[
        Card( 
          color: item['color'],
          child: ListTile(
          leading: Icon(item['icono'], color: Colors.black,),
          title: Text(item['titulo'], style: styleText,),
          onTap: ()=>Navigator.pushNamed( context,'detail', arguments: {'title': item['titulo']}),
          trailing: Icon(Icons.arrow_right, color: Colors.black,),)
      )],);
    }).toList();
  }
  */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      /*
      body: ListView(
        children: <Widget>[
          ListTile(title: Text('Uno')),
          ListTile(title: Text('Dos')),
          ListTile(title: Text('Tres'))
        ],
      ),*/
      body: _lista(),
     /*
     body: ListView(
        children: _crearItemsCorta1(context),
      ),*/
      /*
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          
          children: [
            Image.asset('assets/images/logoC15Land.png', scale: 1.5,),
            Icon(
              Icons.beach_access,
              color: Colors.blue,
              size: 36.0
            )
          ],
          
          children: <Widget>[
            
            FlutterLogo(size: 60.0),
            FlutterLogo(size: 60.0),
            FlutterLogo(size: 60.0)
            
            Icon(Icons.adb, color: Colors.red, size: 50.0),
            Icon(
              Icons.beach_access,
              color: Colors.blue,
              size: 36.0
            )
          ],
        )
      ),*/
      /*
      body: Align(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Uno', style: styleText),
            Text('Dos', style: styleText),
            Text('Tres', style: styleText),
          ],
        )
      ),
      */
      /*
      body: Text(
        'Hola Amigos',
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.blue, fontSize: 25.0),
      ),*/
    );
  }

  Widget _lista() {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(children: _listaItems(snapshot.data, context),);
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, context){
    final List<Widget> opciones = [];

    data.forEach((element) {
      final widgetTemp = Card(
        child: ListTile(
          title: Text(element['texto']),
          leading: getIcon(element['icon']),
          trailing: Tooltip(
            message: "Ir a ${element['texto']}",
            child: IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: () {},
            ),
          ),
          onTap: (){
            print(element['texto']);
            Navigator.pushNamed(context, element['ruta'], arguments: {'title': element['texto']});
          },
        ),
      );
      //opciones..add(widgetTemp)..add(Divider());
      opciones..add(widgetTemp);
    });
    return opciones;
  }
}