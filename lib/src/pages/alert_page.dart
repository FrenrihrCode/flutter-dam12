import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Alert page'),
          centerTitle: false,
      ),
      body: Center(
        child: RaisedButton(
                onPressed: () => _mostrarAlerta,
                color: Colors.blue,
                child: Text("Mostrar alerta"),
                textColor: Colors.white,
                shape: StadiumBorder(),
              )
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: ()=>{
            Navigator.pop(context)
        }),
    );
    /*
    return MaterialApp(
      title: 'MaterialApp',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.cyan[900],
        accentColor: Colors.cyan[600],
        // Define the default font family.
        fontFamily: 'Georgia',
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.cyan[700],     //  <-- dark color
          textTheme: ButtonTextTheme.primary, //  <-- this auto selects the right color
        )
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
          centerTitle: false,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Vista alertas', style: TextStyle(fontSize: 25)),
              RaisedButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                          title: Text('Una alerta'),
                          content: Text('Contenido de la alerta'),
                      )
                  );
                },
                child: Text("Mostrar alerta"),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: ()=>{
            Navigator.pop(context)
        }),
      ),
    );*/
  }

  void _mostrarAlerta(context){
    showDialog(
      context: context,
      builder: (context){
        return AlertDialog(
          title: Text('Titulo'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('Contenido de la caja de alerta'),
              FlutterLogo(size: 100.0)
            ],
          ),
          actions: <Widget>[
            FlatButton(onPressed: (){}, child: Text('Cancelar')),
            FlatButton(onPressed: (){}, child: Text('Cancelar'))
          ],
        );
      }
    );
  }
}