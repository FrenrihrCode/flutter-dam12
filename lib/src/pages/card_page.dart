import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Cards')),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          SizedBox(height: 30.0,),
          _cardTipo2()
        ],
      ),
    );
  }

  Widget _cardTipo1(){
    return Card(
      child: Column(children: <Widget>[
        ListTile(
          title: Text('Soy el título'),
          subtitle: Text('Aca irá una descripción corta'),
          leading: Icon(Icons.photo_album, color: Colors.blue,),),
        Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          FlatButton(onPressed: () {}, child: Text('Cancelar')),
          FlatButton(onPressed: () {}, child: Text('OK')),
        ],)
      ],)
    );
  }

  Widget _cardTipo2(){
    return Card(
      child: Column(children: <Widget>[
        /*
        Image(image: NetworkImage('https://www.fbit.com.mx/wp-content/uploads/2019/12/pixel-art-portada.png')),
        */
        FadeInImage(
          placeholder: AssetImage('assets/images/loading.gif'), 
          image: NetworkImage('https://www.fbit.com.mx/wp-content/uploads/2019/12/pixel-art-portada.png'),
          fadeInDuration: Duration(milliseconds: 200),
          height: 300.0,
          fit: BoxFit.cover,
        ),
        Container(padding: EdgeInsets.all(10.0), child: Text('Un pixel Art.'),)
      ],),
    );
  }
    /*
    return MaterialApp(
      title: 'MaterialApp',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Vista Card', style: TextStyle(fontSize: 25)),
              Card(
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  color: Colors.blue[200],
                  child: Column(
                    children: <Widget>[
                      Text('Hello World'),
                      Text('How are you?')
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          onPressed: ()=>{
            Navigator.pop(context)
        }),
      ),
    );
  }*/
}