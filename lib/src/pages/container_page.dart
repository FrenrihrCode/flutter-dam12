import 'package:flutter/material.dart';

class ContainerPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MaterialApp',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.blueGrey[600],
        accentColor: Colors.white54,
        fontFamily: 'Ewert',
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
          centerTitle: false,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/logoC15Land.png', scale: 1.5,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlutterLogo(size: 60.0),
                  FlutterLogo(size: 60.0),
                  FlutterLogo(size: 60.0),
                ]
              ),
              Icon(Icons.adb, color: Colors.white54, size: 50.0),
            ],
          )
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back),
          onPressed: ()=>{
            Navigator.pop(context)
        }),
      ),
    );
  }
}